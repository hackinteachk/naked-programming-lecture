#  Intro to Python

> Last edit : Wednesday 17 July 2019
>
> site : https://nakedprog.nuttapat.me

[TOC]

## Get ready for Python

Check out tutorial on how to install Python [here](https://nakedprog.nuttapat.me)

## Why do we code ?

​	Computer cannot completely replace human, at least many years from now but it reduces human works so that we can put work forces in a more productive way, hence to increase productivity of the business.

## Python

- Human readable
- Low learning curves
- Powerful (but computationally slow)
- Applications!
  - Machine Learning & AI
  - Data Analyst, Data Scientists
  - Web Application
  - Chatbot
  - Physics simulation and calculation
  - Numerical Calculation
  - Game
  - Excel!
  - etc...

## Development Tools

**IDE** vs **Editor** 

IDE can edit, run, debug, etc.

Editor : edit the code !

### IDEs

- Pycharm
- Spyder
- *Vim / Emacs*

### Code Editor

- Atom
- Visual Studio Code
- *Vim / Emacs*
- Notepad (hopefully not)
- Sublime Text
- Microsoft Word ?

## Hello, World

A very first introduction to programming.

everyone on Earth should heard this word some times in their life.

Try this in your console.

```python
print("hello, world")
```

and this

```python
print(hello, world)
```



### Exercise 1.

print the following text to the screen

> Hello, <your name>

## Variables

**Storage location** for the data in a program. Variable should be named meaningfully ! This can help you debug the code easily!

Convention:

```python
variable_name = variable_value
```

examples:

```python
x = 13
x_plus_10 = x + 10
```

#### Common errors

what is the output of the following code ?

```python
print(naked_programming)
```

> **Undefined variables** : error when you have a typo or you tried to access the value from unknown variable name



```python
class = "nakedProg"
```

>  **Reserved keywords** : names which are used by Python and can not be used by the program. [List of reserved words](https://www.w3schools.com/python/python_ref_keywords.asp)
>
> Note : usually IDE will highlight the keyword for you (it’s called Syntax Highlighting)

### Exercise

Determine the output of the variable `third`:

```python
first = 2
second = 3
third = first * second
second = third - first
first = first + second + third
third = second * first
```

> **Mutable** : ability of the data to be modified

Complete the following exercises : <https://cscircles.cemc.uwaterloo.ca/1-variables/>

## Errors

| Syntax Error              | Runtime Error            |
| ------------------------- | ------------------------ |
| “ Please dog cake beach ” | “ Please eat the piano ” |

Syntax error : Python don’t understand what you are trying to do

Runtime error : It understand what you want to do but there’s trouble running it

> Syntax Error

```python
print(Hello, World!)
class = "Advanced Computronics for Beginners"
name = "Jim
print(77
```

>Runtime Error

```python
callMe = "Maybe"
print(callme)
print(1/0)
print("you cannot add text and numbers" + 12)
```

### Exercise

Complete the exercises and try running each cell : <https://cscircles.cemc.uwaterloo.ca/1e-errors/>

## Comment and Quotes

